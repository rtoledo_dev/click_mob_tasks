# frozen_string_literal: true

Rails.application.routes.draw do
  resources :task_logs
  root to: 'tasks#index'
  resources :tasks
  devise_for :users, path_prefix: 'oauth'
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
