### Instructions to install
``rails db:drop db:create db:migrate db:seed db:test:prepare``
To access the system use the informations that are in db/seeds.rb

### Commands for test

This will generate a new coverage for project
``guard``
Press *ENTER*


### Clear code

This will auto fix the code developed
``rubocop -a --require rubocop-rspec``
