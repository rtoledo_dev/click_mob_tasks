# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Tasks features' do
  before(:all) do
    @user = create(:user)
    @task_example = create(:task, user_id: @user.id)
    @user_example = create(:user)
    create_list(:task, 10, user_id: @user.id)
    @success_message = 'Operação efetuada com sucesso.'
  end

  it 'List of all tasks' do
    visit '/'
    fill_in 'user_email', with: @user.email
    fill_in 'user_password', with: @user.password
    click_button 'Acessar'
    click_link 'Lista de Tarefas'
    expect(page).to have_content('Lista de Tarefas')

    # Implant Datatable result
  end

  it 'Create Task' do
    visit '/'
    fill_in 'user_email', with: @user.email
    fill_in 'user_password', with: @user.password
    click_button 'Acessar'
    click_link('Criar Tarefa', match: :first)

    select @user_example.name, from: 'task[user_id]'
    fill_in 'task_title', with: @task_example.title
    fill_in 'task_description', with: @task_example.description
    click_button 'Salvar'
    expect(page).to have_content(@success_message)
  end

  it 'Fail on Task with blank fields' do
    visit '/'
    fill_in 'user_email', with: @user.email
    fill_in 'user_password', with: @user.password
    click_button 'Acessar'
    click_link('Criar Tarefa', match: :first)

    click_button 'Salvar'
    expect(page).to have_content('Título não pode ficar em branco')
    expect(page).to have_content('Descrição não pode ficar em branco')
  end

  it 'Update Task' do
    visit '/'
    fill_in 'user_email', with: @user.email
    fill_in 'user_password', with: @user.password
    click_button 'Acessar'
    visit edit_task_path(@task_example)

    select @user_example.name, from: 'task[user_id]'
    fill_in 'task_title', with: Faker::Name.unique.name.to_s
    fill_in 'task_description', with: Faker::Lorem.unique.to_s
    click_button 'Salvar'
    expect(page).to have_content(@success_message)
    expect(page).to have_content('Adicionar Log de Anotações')
    expect(page).to have_content('Lista de Anotações')
    expect(page).to have_http_status 200

    expect do
      @task_example.reload
    end.to change(@task_example, :title)
  end

  it 'Fail on Task with empty fields' do
    visit '/'
    fill_in 'user_email', with: @user.email
    fill_in 'user_password', with: @user.password
    click_button 'Acessar'
    visit edit_task_path(@task_example)

    fill_in 'task_title', with: ''
    fill_in 'task_description', with: ''
    click_button 'Salvar'
    expect(page).to have_content('Título não pode ficar em branco')
    expect(page).to have_content('Descrição não pode ficar em branco')
    expect(page).to have_http_status 200
  end
end
