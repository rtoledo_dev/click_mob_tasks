# frozen_string_literal: true

FactoryBot.define do
  factory :task_log do
    association :task, factory: :task
    association :user, factory: :user
    description { Faker::Lorem.unique }
    finished { false }
  end
end
