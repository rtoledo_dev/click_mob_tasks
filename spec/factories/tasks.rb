# frozen_string_literal: true

FactoryBot.define do
  factory :task do
    association :user, factory: :user
    title { Faker::Name.unique.name }
    description { Faker::Lorem.unique }
    scheduled_to { '2018-12-01 09:00:00' }
    status { 'em_andamento' }
  end
end
