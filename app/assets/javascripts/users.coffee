# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
do ->
  usersTable = null

  document.addEventListener 'turbolinks:load', ->
    usersTable = $('#users-datatable').dataTable
      language:
          sEmptyTable: "Nenhum registro encontrado",
          sInfo: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          sInfoEmpty: "Mostrando 0 até 0 de 0 registros",
          sInfoFiltered: "(Filtrados de _MAX_ registros)",
          sInfoPostFix: "",
          sInfoThousands: ".",
          sLengthMenu: "_MENU_ resultados por página",
          sLoadingRecords: "Carregando...",
          sProcessing: "Processando...",
          sZeroRecords: "Nenhum registro encontrado",
          sSearch: "Pesquisar",
          oPaginate:
              sNext: "Próximo",
              sPrevious: "Anterior",
              sFirst: "Primeiro",
              sLast: "Último"
          oAria:
              sSortAscending: ": Ordenar colunas de forma ascendente",
              sSortDescending: ": Ordenar colunas de forma descendente"
      processing: true
      ordering: false
      serverSide: true
      searching: true
      destroy: true
      ajax: $('#users-datatable').data('source')
      pagingType: 'full_numbers'
      columns: [
        {data: 'name'}
        {data: 'email'}
        {data: 'kind_of'}
        {data: 'actions'}
      ]
  document.addEventListener 'turbolinks:before-cache', ->
    if usersTable.length == 1
      usersTable.destroy
