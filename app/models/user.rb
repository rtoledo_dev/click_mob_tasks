# frozen_string_literal: true

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  enum kind_of: %i[administrador operador]

  scope :administrators, -> { where(kind_of: 'administrador') }
  scope :operators, -> { where(kind_of: 'operador') }

  validates :name, presence: true
end
