# frozen_string_literal: true

class Task < ApplicationRecord
  belongs_to :user
  enum status: %i[em_andamento finalizado]

  validates :title, :description, presence: true
  validates_associated :user

  has_many :task_logs
end
