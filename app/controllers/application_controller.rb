# frozen_string_literal: true

class ApplicationController < ActionController::Base
  layout :layout_by_resource

  protected

  def layout_by_resource
    if devise_controller?
      'devise'
    else
      'application'
    end
  end

  def redirect_unless_operator
    redirect_to root_path if current_user.nil? || !current_user.administrador?
  end
end
