# frozen_string_literal: true

json.set! :data do
  json.array! @tasks do |task|
    json.partial! 'tasks/task', task: task
    json.url  "
              #{link_to 'Show', task}
              #{link_to 'Edit', edit_task_path(task)}
              #{link_to 'Destroy', task, method: :delete, data: { confirm: 'Are you sure?' }}
              "
  end
end
