# frozen_string_literal: true

class TaskDatatable < AjaxDatatablesRails::Base
  extend Forwardable

  def_delegators :@view, :params, :link_to, :edit_task_path, :current_user, :task_path

  def view_columns
    @view_columns ||= {
      title: { source: 'Task.title' },
      user: { source: 'User.name' },
      status: { source: 'Task.status', cond: :eq },
      actions: { source: 'User.id', null_value: true }
    }
  end

  def searchable_columns
    @searchable_columns ||= []
  end

  def data
    records.map do |record|
      {
        title: record.title,
        user: record.user.name,
        status: record.status.humanize,
        actions: action_columns(record)
      }
    end
  end

  def action_columns(task)
    actions = ['<div class="btn btn-group" aria-label="Ações">']
    actions << link_to(I18n.t('edit_label'), edit_task_path(task), class: 'btn btn-sm btn-primary')
    actions << link_to(I18n.t('destroy_label'), task_path(task), method: :delete, data: { confirm: I18n.t('destroy_object') }, class: 'btn btn-sm btn-danger') if current_user.administrador?
    actions << ['</div>']
    actions.join.html_safe
  end

  def get_raw_records
    tasks = Task.order('updated_at DESC')

    tasks = tasks.joins(:user).where('users.id = ?', current_user.id) unless current_user.administrador?

    if !params[:search].blank? && !params[:search][:value].blank?
      search = params[:search][:value]
      tasks = tasks.joins(:user).where('(tasks.title ILIKE :search OR users.name ILIKE :search)', search: "#{search}%")
    end

    tasks
  end
end
