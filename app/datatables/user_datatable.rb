# frozen_string_literal: true

class UserDatatable < AjaxDatatablesRails::Base
  extend Forwardable

  def_delegators :@view, :params, :link_to, :edit_user_path, :user_path, :current_user

  def view_columns
    @view_columns ||= {
      name: { source: 'User.name' },
      email: { source: 'User.email' },
      kind_of: { source: 'User.kind_of', cond: :eq },
      actions: { source: 'User.id', null_value: true }
    }
  end

  def searchable_columns
    @searchable_columns ||= []
  end

  def data
    records.map do |record|
      {
        name: record.name,
        email: record.email,
        kind_of: record.kind_of.humanize,
        actions: action_columns(record)
      }
    end
  end

  def action_columns(user)
    actions = ['<div class="btn btn-group" aria-label="Ações">']
    actions << link_to(I18n.t('edit_label'), edit_user_path(user), class: 'btn btn-sm btn-primary')
    actions << link_to(I18n.t('destroy_label'), user_path(user), method: :delete, data: { confirm: I18n.t('destroy_object') }, class: 'btn btn-sm btn-danger') if user.id != current_user.id
    actions << ['</div>']
    actions.join.html_safe
  end

  def get_raw_records
    users = User.order('updated_at DESC')

    if !params[:search].blank? && !params[:search][:value].blank?
      search = params[:search][:value]

      users = users.where('(name ILIKE :search OR email ILIKE :search)', search: "%#{search}%")
    end

    users
  end
end
