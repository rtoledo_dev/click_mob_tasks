# frozen_string_literal: true

# tasks to be assigned to one user
class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.references :user, foreign_key: true
      t.string :title
      t.text :description
      t.datetime :scheduled_to
      t.integer :status

      t.timestamps
    end
  end
end
