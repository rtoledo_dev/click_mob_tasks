# frozen_string_literal: true

class ChangeColumnStatusInTaskLogs < ActiveRecord::Migration[5.2]
  def change
    rename_column :task_logs, :status, :finished
  end
end
